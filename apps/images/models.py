# Python
from datetime import datetime

from mongoengine import (
    DateTimeField,
    StringField,
    IntField
)


# Apps
from apps.db import db


class Image(db.Document):
    """
        Images
    """

    meta = {
        'collection': 'images'
    }
    camera = StringField(required=True)
    status = IntField(default=0)
    image = StringField(required=True)
    created = DateTimeField(default=datetime.now)
