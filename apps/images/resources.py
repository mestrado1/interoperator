# -*- coding: utf-8 -*-

# Python

# Flask
from flask import request

# Third
from flask_restful import Resource
from mongoengine.errors import FieldDoesNotExist

# Apps
from apps.responses import (
    resp_data_invalid,
    resp_exception,
    resp_post_ok,
    resp_get_ok
)
from apps.messages import (
    MSG_NO_DATA,
    MSG_INVALID_DATA,
    MSG_RESOURCE_CREATED,
    MSG_RESOURCE_FETCHED
)

# Local
from .models import Image


class ImageResource(Resource):

    def post(self, *args, **kwargs):
        # Inicializar todas as variáveis utilizads
        req_data = request.get_json() or None

        if req_data is None:
            return resp_data_invalid('Images', [], msg=MSG_NO_DATA)

        if 'camera' not in req_data or 'image' not in req_data:
            return resp_data_invalid('Images', [], msg=MSG_INVALID_DATA)

        try:
            model = Image()
            model.camera = req_data['camera']
            model.image = req_data['image']
            model.save()
        except Exception as e:
            return resp_exception('Images', description=e)

        # Retorno 201 o meu endpoint
        return resp_post_ok(
            'Image', MSG_RESOURCE_CREATED.format('Image'),  data=model,
        )

    def get(self):
        try:
            # buscamos todos as imagens
            images = Image.objects()
        except FieldDoesNotExist as e:
            return resp_exception('Images', description=e.__str__())

        except Exception as e:
            return resp_exception('Images', description=e.__str__())

        return resp_get_ok(
            'Images', MSG_RESOURCE_FETCHED.format('images'),  data=images
        )


class ImageResourceEspecial(Resource):
    def get(self, limit=1):
        try:
            # buscamos todos as imagens da base utilizando o limit
            images = Image.objects().limit(limit)
        except FieldDoesNotExist as e:
            return resp_exception('Images', description=e.__str__())

        except Exception as e:
            return resp_exception('Images', description=e.__str__())

        return resp_get_ok(
            'Images', MSG_RESOURCE_FETCHED.format('images'),  data=images
        )
