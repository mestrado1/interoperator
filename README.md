# Interoperator

API para fazer a intercomunicação entre os módulos de Detector e Identifier. 


## Dependências
    Flask
    Pytest


## Informações úteis para a execução do projeto

* Criar arquivo de configuração .env na raiz do 
projeto. Pode baser-se no .env-example

* Criar arquivo de configuração config.py na raiz do projeto. Pode baser-se no config-example.py

* Instalar dependências do projeto
        
    ```bash
    # Teste
    $ pip install -r requirements/test.txt
    ```
    
    ```bash
    # Desenvolvimento
    $ pip install -r requirements/dev.txt
    ```
    
    ```bash
    # Produção
    $ pip install -r requirements/prod.txt
    ```

* Executar Projeto
    
    > A execução do projeto é baseada no Makefile, sendo os dois principais comandos elencados abaixo

    ```bash
    # Executa a aplicação web em desenvolvimento, com os testes e cobertura"
    $ make dev
    ```

    ```bash
    # Executa a aplicação web sem os testes
	$ make run
    ```
    > Para mais outros comandos consulte o arquivo Makefile na raiz do projeto.

* Executar testes
    > Antes de executar o teste que criamos, execute o comando abaixo para instalar o pacote API e termos referência para executarmos o teste com nossa suite de testes. [Clique aqui para mais informações](https://docs.pytest.org/en/latest/existingtestsuite.html)

    ```bash
    $ pip install -e .
    ```
    
    ```bash
    $ pytest -v
    ```

    > Também é possível executar testes diretamente via linha de comando:

    ```bash
    # Executa todos os testes da pasta auth.
    $ pytest tests/auth/
    ```
    > 

    ```bash
    # Executa todos os testes do módulo test_resources.
    $ pytest tests/auth/test_resources.py
    ```

    ```bash
    # Executa todos os teste da classe TestAuthenticateUser.
    $ pytest tests/auth/test_resources.py::TestAuthenticateUser
    ```

    ```bash
    # Executa o teste específico chamado test_raise_exception_payload_is_empty.
    $ pytest tests/auth/test_resources.py::TestAuthenticateUser::test_raise_exception_payload_is_empty
    ```

## Bibliografia

[Tutorial de Construção da API](https://lucassimon.com.br/2018/06/serie-api-em-flask---parte-3---configurando-o-pytest-e-nosso-primeiro-teste/)