# -*- coding: utf-8 -*-

from flask import jsonify
from .messages import MSG_INVALID_DATA, MSG_EXCEPTION


def resp_get_ok(resource: str, message: str, data=None, **extras):
    '''
    Responses 200
    '''

    response = {'status': 200, 'message': message, 'resource': resource}

    if data:
        response['data'] = data

    response.update(extras)

    resp = jsonify(response)

    resp.status_code = 200

    return resp


def resp_post_ok(resource: str, message: str, data=None, **extras):
    '''
    Responses 201
    '''
    response = {'status': 201, 'message': message, 'resource': resource}

    if data:
        response['data'] = data

    response.update(extras)

    resp = jsonify(response)

    resp.status_code = 201

    return resp


def resp_data_invalid(resource: str, errors: dict, msg: str = None):

    '''
    Responses 422 Unprocessable Entity
    '''

    if not isinstance(resource, str):
        raise ValueError('O recurso precisa ser uma string.')

    resp = jsonify({
        'resource': resource,
        'message': msg if msg else MSG_INVALID_DATA,
        'errors': errors,
    })

    resp.status_code = 422

    return resp


def resp_exception(resource: str, description: str = '', msg: str = None):
    '''
    Responses 500
    '''

    if not isinstance(resource, str):
        raise ValueError('O recurso precisa ser uma string.')

    resp = jsonify({
        'resource': resource,
        'message': msg if msg else MSG_EXCEPTION,
        'description': description
    })

    resp.status_code = 500

    return resp
