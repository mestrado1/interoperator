from setuptools import find_packages, setup

__version__ = '1.0.0'
__description__ = 'Interoperator'
__long_description__ = 'This is an API to Facial'

__author__ = 'Guilherme Figueiredo Terenciani'
__author_email__ = 'guilherme.terenciani@ifms.edu.br'

testing_extras = [
    'pytest',
    'pytest-cov',
]

setup(
    name='interoperator',
    version=__version__,
    author=__author__,
    author_email=__author_email__,
    packages=find_packages(),
    license='MIT',
    description=__description__,
    long_description=__long_description__,
    url='',
    keywords='API, MongoDB',
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Operating System :: OS Independent',
        'Topic :: Software Development',
        'Environment :: Web Environment',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'License :: OSI Approved :: MIT License',
    ],
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    extras_require={
        'testing': testing_extras,
    }
)
