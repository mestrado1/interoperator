# Python
import json
# import pytest

# Apps

from apps.images.models import Image


class TestImage:
    def setup_method(self):
        self.data = {
            'camera': 'minha camera 1',
            'image': 'img'
        }

        self.invalid_data = {
            'cam': 'minha camera 1',
            'img': 'img'
        }

        self.model = Image(**self.data)

    def test_image_post_invalid_params(self, client):
        """
            Realiza uma requisição HTTP do tipo GET para o endpoint '/images'
        """

        response = client.post('/images', json=self.invalid_data)

        # Verificamos a assertividade do código de resposta da requisição
        # http. Ela deve ser exatamente igual 200 retornando um True para
        # o teste
        assert response.status_code == 422

    def test_image_post_response_201(self, client):
        """
            Realiza uma requisição HTTP do tipo POST para o endpoint '/images'
        """

        response = client.post('/images', json=self.data)

        # Verificamos a assertividade do código de resposta da requisição
        # http. Ela deve ser exatamente igual 201 retornando um True para
        # o teste
        assert response.status_code == 201

    def test_image_post_response_has_id(self, client, mongo):
        """
            Realiza uma requisição HTTP do tipo POST para o endpoint '/image'
        """

        response = client.post('/images', json=self.data)

        json_data = json.loads(response.data)

        data = json_data['data']

        # Verificamos a assertividade do código de resposta da requisição
        # http. Ela deve ser exatamente igual 201 e na resposta deve
        # existir id, indicando a informação foi salva corretamente
        assert response.status_code == 201 and '_id' in data

    def test_image_post_empty_params(self, client):
        """
            Realiza uma requisição HTTP do tipo POST para o endpoint '/image'
            com os parâmetros em branco
        """

        response = client.post('/images', None)

        # Verificamos a assertividade do código de resposta da requisição
        # http. Ela deve ser exatamente igual 422 retornando um True para
        # o teste
        assert response.status_code == 422

    def test_image_get_limit_zero(self, client):
        """
            Realiza uma requisição HTTP do tipo POST para o endpoint '/image'
            com limit == 0
        """

        response = client.get('/images/0')
        json_data = json.loads(response.data)

        len_data = len(json_data['data'])

        # Verificamos a assertividade do código de resposta da requisição
        # http. Ela deve ser exatamente igual 200 retornando um True para
        # o teste
        assert response.status_code == 200 and len_data == 0

    def test_image_get_limit_less_equal_two(self, client):
        """
            Realiza uma requisição HTTP do tipo POST para o endpoint '/image'
            com limit == 2
        """

        response = client.get('/images/2')
        json_data = json.loads(response.data)

        len_data = len(json_data['data'])

        # Verificamos a assertividade do código de resposta da requisição
        # http. Ela deve ser exatamente igual 200 retornando um True para
        # o teste e o tamanho da resposta tem que ser menor ou igual que dois
        assert response.status_code == 200 and len_data <= 2

    def test_image_get_response_200(self, client):
        """
            Realiza uma requisição HTTP do tipo GET para o endpoint '/images'
        """

        response = client.get('/images')

        # Verificamos a assertividade do código de resposta da requisição
        # http. Ela deve ser exatamente igual 200 retornando um True para
        # o teste
        assert response.status_code == 200
